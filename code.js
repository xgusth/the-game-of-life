(function () {
    // global variables
    var rows = 50;
    var cols = 100;

    var playing = false;

    var grid = new Array(rows);
    var nextGrid = new Array(rows);

    var timer;
    var reproductionTime = 100;

    // initialize the grids
    function initializeGrids() {
        for (var i = 0; i < rows; i++) {
            grid[i] = new Array(cols);
            nextGrid[i] = new Array(cols);
        }
    }

    // reset the grids to dead cells
    function resetGrids() {
        for (var i = 0; i < rows; i++) {
            for (var j = 0; j < cols; j++) {
                grid[i][j] = 0;
                nextGrid[i][j] = 0;
            }
        }
    }

    // copy the next grid to the current grid and reset the next grid
    function copyAndResetGrid() {
        for (var i = 0; i < rows; i++) {
            for (var j = 0; j < cols; j++) {
                grid[i][j] = nextGrid[i][j];
                nextGrid[i][j] = 0;
            }
        }
    }

    // initialize
    function initialize() {
        createTable();
        initializeGrids();
        resetGrids();
        setupControlButtons();
    }

    // lay out the board
    function createTable() {
        var gridContainer = document.getElementById('gridContainer');
        var table = document.createElement('table');

        for (var i = 0; i < rows; i++) {
            var tr = document.createElement('tr');
            for (var j = 0; j < cols; j++) {
                var cell = document.createElement('td');
                cell.setAttribute('id', i + '_' + j);
                cell.setAttribute('class', 'dead');
                cell.onclick = cellClickHandler;
                tr.appendChild(cell);
            }
            table.appendChild(tr);
        }

        gridContainer.appendChild(table);
    }

    // handle the cell click
    function cellClickHandler() {
        var classes = this.getAttribute('class');
        var rowcol = this.id.split('_');
        var row = rowcol[0];
        var col = rowcol[1];

        if (classes.indexOf('live') > -1) {
            this.setAttribute('class', 'dead');
            grid[row][col] = 0;
        } else {
            this.setAttribute('class', 'live');
            grid[row][col] = 1;
        }
    }

    // update the view based on the current grid
    function updateView() {
        for (var i = 0; i < rows; i++) {
            for (var j = 0; j < cols; j++) {
                var cell = document.getElementById(i + '_' + j);

                if (grid[i][j] == 0) {
                    cell.setAttribute('class', 'dead');
                } else {
                    cell.setAttribute('class', 'live');
                }
            }
        }
    }

    // set up the control buttons
    function setupControlButtons() {
        var startButton = document.getElementById('start');
        startButton.onclick = startButtonHandler;

        var clearButton = document.getElementById('clear');
        clearButton.onclick = clearButtonHandler;

        var randomButton = document.getElementById('random');
        randomButton.onclick = randomButtonHandler;
    }

    // handle the clear button
    function clearButtonHandler() {
        playing = false;

        var startButton = document.getElementById('start');
        startButton.innerHTML = 'start';

        var cellsList = document.getElementsByClassName('live');
        while (cellsList.length > 0) {
            cellsList[0].setAttribute('class', 'dead');
        }

        resetGrids();
    }

    // handle the start button
    function startButtonHandler() {
        if (playing) {
            playing = false;
            this.innerHTML = 'continue';
            clearTimeout(timer);
        } else {
            playing = true;
            this.innerHTML = 'pause';
            play();
        }
    }

    // handle the random button
    function randomButtonHandler() {
        if (playing) {
            return;
        }

        clearButtonHandler();

        for (var i = 0; i < rows; i++) {
            for (var j = 0; j < cols; j++) {
                var isLive = Math.round(Math.random());

                if (isLive == 1) {
                    var cell = document.getElementById(i + '_' + j);
                    cell.setAttribute('class', 'live');
                    grid[i][j] = 1;
                }
            }
        }
    }

    // run the game
    function play() {
        computeNextGen();

        if (playing) {
            timer = setTimeout(play, reproductionTime);
        }
    }

    // compute the next grid generation
    function computeNextGen() {
        for (var i = 0; i < rows; i++) {
            for (var j = 0; j < cols; j++) {
                applyRules(i, j);
            }
        }

        copyAndResetGrid();
        updateView();
    }

    // apply the rules of the Game of Life to a single cell
    function applyRules(row, col) {
        var numNeighbors = countNeighbors(row, col);

        if (grid[row][col] == 1 && numNeighbors < 2) {
            // Rule #1, live cell with less than two neighbors die by under-population
            nextGrid[row][col] = 0;
        }

        if (grid[row][col] == 1 && (numNeighbors == 2 || numNeighbors == 3)) {
            // Rule #2, live cell continues lived if has two or three neighboors
            nextGrid[row][col] = 1;
        }

        if (grid[row][col] == 1 && numNeighbors > 3) {
            // Rule #3, live cell with three or more neighbors die by overcrowding
            nextGrid[row][col] = 0;
        }

        if (grid[row][col] == 0 && numNeighbors == 3) {
            // Rule #4, dead cell with three neighbors live by reproduction
            nextGrid[row][col] = 1;
        }
    }

    // count how many live neighbors a cell have
    function countNeighbors(row, col) {
        var count = 0;

        if (row-1 >= 0 && col-1 >= 0 && grid[row-1][col-1] == 1) count++; // top-left
        if (row-1 >= 0 && grid[row-1][col] == 1) count++; // top
        if (row-1 >= 0 && col+1 < cols && grid[row-1][col+1] == 1) count++; // top-right
        if (col-1 >= 0 && grid[row][col-1] == 1) count++; // left
        if (col+1 < cols && grid[row][col+1] == 1) count++; // right
        if (row+1 < rows && col-1 >= 0 && grid[row+1][col-1] == 1) count++; // bottom-left
        if (row+1 < rows && grid[row+1][col] == 1) count++; // bottom
        if (row+1 < rows && col+1 < cols && grid[row+1][col+1] == 1) count++; // bottom-right

        return count;
    }

    // start everything
    window.onload = initialize;
})();
